import os

yOffset = '5.75cm'
xOffset = '-4.5cm'
scale   = '0.3'

label_type = "WIP"
#label_type = "Internal"
#label_type = "Preliminary"
#label_type = "Simulation"
#label_type = "Simulation_Internal"
#label_type = "Simulation_WIP"

def addLabel(inFile, yOffset, xOffset, scale, label_type):

    # Input File
    os.system("pdfjam --fitpaper true input/%s.pdf" % inFile)

    # ATLAS Label
    os.system("pdfjam  --papersize '{10in,18cm}' --scale %s --offset '%s %s' labels/label_%s.pdf" % (scale, xOffset, yOffset, label_type))

    # Merge 
    os.system("pdftk %s-pdfjam.pdf stamp label_%s-pdfjam.pdf output output/%s.pdf" % (inFile, label_type, inFile))

    # Not needed, but in case you want to crop the output file:
    #os.system("pdfcrop --margins '19 8 15 6' output/%s.pdf output/%s.pdf" % (inFile, inFile))

    # Cleanup
    os.system("rm *-pdfjam.pdf")
    

inFiles = [f for f in os.listdir('input') if os.path.isfile(os.path.join('input', f))]

for f in inFiles:
    f = f[:-4]
    addLabel(f, yOffset, xOffset, scale, label_type)
