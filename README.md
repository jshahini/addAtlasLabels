# addATLASLabels

Scenario: Your thesis is due in one week. Your advisor is complaining that plots you made three years ago do not have nice ATLAS labels. What to do???

You're in luck. The `addLabel.py` script will add a variety of ATLAS labels to your plots with your desired size and location.

**Requirements:**
- Your input plots must be pdfs
- Install `pdfjam` and `pdftk`

**Usage:**
- `mkdir input output`
- Put your plots in the `input` directory
- Run the python script: `python addLabel.py`
- Then your stamped output plots (also pdfs) will be in the `output` directory
- Inside `addLabel.py`, you can control the size and position of the labels with `scale`, `xOffset`, and `yOffset`
- The following labels are supported, and controlled by `label_type`
  * "Work in Progress"
  * "Internal"
  * "Preliminary"
  * "Simulation"
  * "Simulation Internal"
  * "Simulation Work in Progress"
- A label of simply "ATLAS" is **not supported** because this breaks the rules about plot approval. Do not simply add "ATLAS" to unapproved plots. That is no bueno. 